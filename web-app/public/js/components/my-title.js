Vue.component(`my-title`, {
  template: `
    <div>
      <h1>
        {{title}}
      </h1> 
    </div>
  `,
  data() {
    return {
      title: "Hello World",
    }
  },
  methods:{

  },
  mounted() {
    console.log("⚡️ the my-title vue is mounted")
    this.$root.$on("ping", (message)=> {
      console.log(`🎉 ${message} loaded`)
    })
  }
});



